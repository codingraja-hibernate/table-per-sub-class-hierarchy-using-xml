package com.codingraja.domain;

public class LoanAccount extends Account {
	private Double intrestRate;
	private Double emi;
	private Double outStdAmount;
	
	public LoanAccount(){}
	public LoanAccount(Double balance, Long customerId, Double intrestRate, Double emi, Double outStdAmount) {
		super(balance, customerId);
		this.intrestRate = intrestRate;
		this.emi = emi;
		this.outStdAmount = outStdAmount;
	}
	
	public Double getIntrestRate() {
		return intrestRate;
	}
	public void setIntrestRate(Double intrestRate) {
		this.intrestRate = intrestRate;
	}
	public Double getEmi() {
		return emi;
	}
	public void setEmi(Double emi) {
		this.emi = emi;
	}
	public Double getOutStdAmount() {
		return outStdAmount;
	}
	public void setOutStdAmount(Double outStdAmount) {
		this.outStdAmount = outStdAmount;
	}
}
