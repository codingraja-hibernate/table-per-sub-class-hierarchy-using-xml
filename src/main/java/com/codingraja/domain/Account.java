package com.codingraja.domain;

public class Account {
	private Long accNo;
	private Double balance;
	private Long customerId;
	
	public Account(){}
	public Account(Double balance, Long customerId) {
		super();
		this.balance = balance;
		this.customerId = customerId;
	}
	
	public Long getAccNo() {
		return accNo;
	}
	public void setAccNo(Long accNo) {
		this.accNo = accNo;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
}
